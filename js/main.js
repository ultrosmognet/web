
const secondsPerTrackingStep = 10;

window.onload = function(){
	const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
	const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))

	var sourceLocation = document.getElementById("source-location1");
	var destLocation = document.getElementById("dest-location1");
	setupLocationPicker(sourceLocation);
	setupLocationPicker(destLocation);

	locationPicked("source-location2", sourceLocation);
	locationPicked("dest-location2", destLocation);
};

function selectViewerTab() {

	var trackingNumberInputHome = document.getElementById("tracking-number-input-home");
	var trackingNumberInput = document.getElementById("tracking-number-input");
	trackingNumberInput.value = trackingNumberInputHome.value;
	trackingNumberInputHome.value = '';

	var tabElem = document.getElementById("nav-viewer-tab");
	const tabTrigger = new bootstrap.Tab(tabElem);
	tabTrigger.show();
}

function setupLocationPicker(selectElement) {
	for (var id of Object.keys(locations).sort()) {
		if (id.endsWith("00")) {
			selectElement.options[selectElement.options.length] = new Option(locations[id], id);
		}
	}
}

function locationPicked(nextElemId, self) {
	var selectElement1 = self;
	var selectElement2 = document.getElementById(nextElemId);
	var options = selectElement1.options;
	var id      = options[options.selectedIndex].value;
	var regionId = id.substr(0, 2);

	selectElement2.options.length = 0;
	for (var areaId of Object.keys(locations).sort()) {
		if (areaId.startsWith(regionId) && areaId.endsWith("0") && areaId !== id) {
			selectElement2.options[selectElement2.options.length] = new Option(locations[areaId], areaId);
		}
	}
}

function createTrackingNumber() {
	var sourceLocation = document.getElementById("source-location2");
	var destLocation = document.getElementById("dest-location2");

	var options1 = sourceLocation.options;
	var id1      = options1[options1.selectedIndex].value;

	var options2 = destLocation.options;
	var id2      = options2[options2.selectedIndex].value;

	var timestamp = Math.floor(Date.now() / 1000).toString();
	var trackingNumber = encodeTrackingNumber(timestamp, id1, id2);

	var trackingNumberOutput = document.getElementById("tracking-number-output");
	trackingNumberOutput.value = trackingNumber;
}

function viewTrackingNumber() {
	var trackingNumberInput = document.getElementById("tracking-number-input");
	var trackingNumber = trackingNumberInput.value;

	// todo: validate tracking number

	const [timestamp, sourceId, destId] = decodeTrackingNumber(trackingNumber);
	var timeSinceOrdered = Math.floor(Date.now() / 1000) - timestamp;
	var sourceLocation = locations[sourceId];
	var destLocation = locations[destId];
	
	var sourceIdOutput = document.getElementById("source-id-output");
	var destIdOutput = document.getElementById("dest-id-output");

	sourceIdOutput.value = sourceLocation;
	destIdOutput.value = destLocation;

	var trackingProgress = document.getElementById("tracking-progress");
	trackingProgress.classList.remove('d-none');

	var trackingProgressBar = document.getElementById("tracking-progress-bar");
	var pcg = Math.min(100, Math.floor(timeSinceOrdered / secondsPerTrackingStep, 0, 100));
	trackingProgressBar.setAttribute('aria-valuenow', pcg);
	trackingProgressBar.setAttribute('style', `width:${pcg}%`);

	var modalTrackingSteps = document.getElementById('modal-tracking-steps');
	modalTrackingSteps.innerHTML = "";

	if (pcg >= 0) {
		setButtonToPrimary("tracking-progress-btn1");
		if (pcg < 50) {
			emboldenText('tracking-progress-label1');
		}
		document.getElementById('status-header').innerText = 'Ordered';
		document.getElementById('status-subheader').innerText = 'Carrier picked up the package.';
		appendTrackingStep(timestamp, 0, 'Carrier picked up the package.');
	}
	if (pcg >= 50) {
		setButtonToPrimary("tracking-progress-btn2");
		if (pcg < 100) {
			emboldenText('tracking-progress-label2');
		}
		document.getElementById('status-header').innerText = 'Shipped';
		document.getElementById('status-subheader').innerText = 'Package left the carrier facility.';
		appendTrackingStep(timestamp, 50, 'Package left the carrier facility.');
	}
	if (pcg >= 100) {
		setButtonToPrimary("tracking-progress-btn3");
		emboldenText('tracking-progress-label3');
		document.getElementById('status-header').innerText = 'Out for Delivery';
		document.getElementById('status-subheader').innerText = 'Your shit is coming hold the fuck on.';
		appendTrackingStep(timestamp, 100, 'Out for delivery.');
	}

	// Sundays are for Enthrallment
	if (new Date().getDay() === 0) {
		document.getElementById('status-header').innerText = 'Delayed';
		document.getElementById('status-subheader').innerText = 'Sundays are for Enthrallment.';
	}

	document.getElementById("modal-tracking-id").innerText = `Tracking ID: ${trackingNumber}`;
}

function interleveString(s1, s2) {
	var result = '';
	var minLength = Math.min(s1.length, s2.length);
	for (var i = 0; i < minLength; i++){
		result += s1[i];
		result += s2[i];
	}
	
	for (var i = minLength; i < s1.length; i++){
		result += s1[i];
	}
	
	for (var i = minLength; i < s2.length; i++){
		result += s2[i];
	}

	return result;
}

function encodeTrackingNumber(timestamp, sourceId, destId) {
	var locationId = `${sourceId}${destId}`;
	return interleveString(timestamp, locationId);
}

function decodeTrackingNumber(trackingNumber) {
	var timestamp = '';
	var sourceId = '';
	var destId = '';

	// 106851305132323026
	timestamp = [0,2,4,6,8,10,12,14,16,17,18].map(i => trackingNumber[i]).join('');
	sourceId = [1,3,5,7].map(i => trackingNumber[i]).join('');
	destId = [9,11,13,15].map(i => trackingNumber[i]).join('');

	return [Number(timestamp), sourceId, destId];
}

function setButtonToPrimary(id) {
	var btn = document.getElementById(id);
	btn.classList.remove('btn-secondary');
	btn.classList.add('btn-primary');
	btn.innerHTML = '&#10003;';
}

function emboldenText(id) {
	var span = document.getElementById(id);
	span.classList.add('fw-bold');
}

function appendTrackingStep(timestamp, pcg, text) {
	var stepTimestamp = (timestamp + (secondsPerTrackingStep * pcg)) * 1000;
	var date = new Date(stepTimestamp);
	var dateString = date.toLocaleDateString();
	// var seed = cyrb128(stepTimestamp.toString());
	// var rand = mulberry32(seed[0]);

	// var locationIds = Object.keys(locations).sort();
	// var locationIndex = Math.floor(rand() * locationIds.length);
	// console.log(locationIndex, locationIds[locationIndex], locations[locationIds[locationIndex]]);

	// // loop until it's not a regionID
	// while (locationIds[locationIndex].toString().endsWith('00')) {
	// 	console.log(locationIndex, locationIds[locationIndex], locations[locationIds[locationIndex]]);
	// 	locationIndex = (locationIndex + 1) % locationIds.length;
	// }

	// var locationId = locationIds[locationIndex];
	// var locationName = locations[locationId];
	// var regionId = locationId.substring(0, 2) + '00';
	// var regionName = locations[regionId];

	var modalTrackingSteps = document.getElementById('modal-tracking-steps');
	var step = document.createElement('li');
	step.classList.add('d-flex', 'mb-3');

	// vertical line
	var vr = document.createElement('div');
	vr.classList.add('vr', 'mx-5');

	// add italic text location names
	// var locationNode = document.createElement('span');
	// locationNode.innerText = `${locationName}, ${regionName}`;
	// locationNode.classList.add('fst-italic', 'text-muted');

	step.appendChild(document.createTextNode(date.toLocaleTimeString()));
	step.appendChild(vr);
	step.appendChild(document.createTextNode(text));
	// step.appendChild(document.createElement('br'));
	// step.appendChild(locationNode);

	if (modalTrackingSteps.firstChild) {
		modalTrackingSteps.insertBefore(step, modalTrackingSteps.firstChild);
	}
	else {
		modalTrackingSteps.appendChild(step);
	}
}

function showMapLocationPopover(locationNumber) {
	popover = bootstrap.Popover.getInstance(`#location${locationNumber}`);
	popover.show();
}
